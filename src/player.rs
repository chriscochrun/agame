use bevy::prelude::*;
use bevy_third_person_camera::*;

pub struct PlayerPlugin;

impl Plugin for PlayerPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(Startup, spawn_player)
            .add_systems(Update, player_movement);
    }
}

#[derive(Default, Component, Debug)]
struct Player {
    x:      f32,
    y:      f32,
    health: i32,
}

// #[derive(Resource)]
// struct PlayerAnimations {
//     animations: Vec<AnimationNodeIndex>,
//     graph:      Handle<AnimationGraph>,
// }

#[derive(Component)]
struct Speed(f32);

impl Player {
    fn new(x: f32, y: f32, health: i32) -> Self {
        Self { x, y, health }
    }
}

fn player_movement(
    keys: Res<ButtonInput<KeyCode>>,
    time: Res<Time>,
    mut player_query: Query<(&mut Transform, &Speed), With<Player>>,
    mut camera_query: Query<&mut Transform, (With<Camera3d>, Without<Player>)>,
) {
    for (mut player_transform, player_speed) in player_query.iter_mut() {
        let mut cam = match camera_query.get_single_mut() {
            Ok(c) => c,
            Err(e) => Err(format!("Error getting camera: {}", e)).unwrap(),
        };

        let mut direction = Vec3::ZERO;

        if keys.pressed(KeyCode::KeyE) {
            direction += *cam.forward();
            info!(?direction);
        }

        if keys.pressed(KeyCode::KeyD) {
            direction += *cam.back();
            info!(?direction);
        }

        if keys.pressed(KeyCode::KeyS) {
            direction += *cam.left();
            info!(?direction);
        }

        if keys.pressed(KeyCode::KeyF) {
            direction += *cam.right();
            info!(?direction);
        }

        direction.y = 0.0;
        let movement = direction.normalize_or_zero() * player_speed.0 * time.delta_seconds();
        player_transform.translation += movement;
        cam.translation += movement;
    }
}

fn spawn_player(mut commands: Commands, asset_server: Res<AssetServer>) {
    let mut player = (
        SceneBundle {
            scene: asset_server.load("wizard.glb#Scene0"),
            transform: Transform::from_xyz(0.0, 0.0, 0.0),
            ..default()
        },
        Player::new(0.0, 0.0, 200),
        Speed(3.0),
        ThirdPersonCameraTarget,
    );
    player.0.transform.scale = Vec3::from_array([0.5, 0.5, 0.5]);
    player.0.transform.rotate_y(3.9);
    commands.spawn(player);
}
