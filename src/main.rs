mod movement;
mod player;
mod world;

use bevy::prelude::*;
use bevy_third_person_camera::*;
use movement::MovementPlugin;
use player::PlayerPlugin;
use world::WorldPlugin;

fn main() {
    let timer =
        tracing_subscriber::fmt::time::ChronoLocal::new("%Y-%m-%d_%I:%M:%S%.6f %P".to_owned());
    let subscriber = tracing_subscriber::FmtSubscriber::builder()
        .pretty()
        .with_line_number(true)
        .with_target(true)
        .with_timer(timer)
        .with_env_filter("agame=debug,info")
        .finish();

    tracing::subscriber::with_default(subscriber, || {
        App::new()
            .add_plugins((
                DefaultPlugins,
                WorldPlugin,
                MovementPlugin,
                PlayerPlugin,
                ThirdPersonCameraPlugin,
            ))
            .run();
    });
}
